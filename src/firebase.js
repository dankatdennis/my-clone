import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBlP0tMKvmhRp6N1yh15iVIAzOhqE4P1iE",
    authDomain: "whatsapp-91a56.firebaseapp.com",
    databaseURL: "https://whatsapp-91a56.firebaseio.com",
    projectId: "whatsapp-91a56",
    storageBucket: "whatsapp-91a56.appspot.com",
    messagingSenderId: "818813858679",
    appId: "1:818813858679:web:285343745d3bbbd72ca772",
    measurementId: "G-XMFZ42P14M"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export {auth, provider}
export default db;